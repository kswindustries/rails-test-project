class BreedsController < ApplicationController

  def index
    @breed = DogBreedFetcher.fetch
  end

  def show
    @breed = Breed.find(params[:id])
  end

  def create
    breed = DogBreedFetcher.fetch(params[:breed_name])
    redirect_to breed_path(breed.id)
  end

end
