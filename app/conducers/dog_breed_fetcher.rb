class DogBreedFetcher
  attr_reader :breed

  BASE_URL = "https://dog.ceo/api".freeze

  def initialize(name=nil)
    @name  = name || "random"
    @breed = Breed.find_or_initialize_by(name: name)
  end

  def fetch
    return @breed if @breed.pic_url.present?

    @breed.pic_url = fetch_info["message"]
    @breed.save && @breed
  end

  def self.fetch(name=nil)
    name ||= "random"
    DogBreedFetcher.new(name).fetch
  end

private
  def fetch_info
    begin
      JSON.parse(RestClient.get(url).body)
    rescue Object => e
      default_body
    end
  end

  def url
    @name == "random" ? random_url : sub_breed_url
  end

  def sub_breed_url
    "#{BASE_URL}/breed/#{slug}/images/random"
  end

  def random_url
    "#{BASE_URL}/breeds/image/random"
  end

  def slug
    names = @name.split(" ").reverse.map(&:downcase)
    names.join("/")
  end

  def default_body
    {
      "status"  => "success",
      "message" => "https://images.dog.ceo/breeds/cattledog-australian/IMG_2432.jpg"
    }
  end
end
