module DogBreedNameList
  extend self

  def fetch
    names = fetch_names
    if success?(names)
      build_list_for(names["message"])
    else
      build_list_for(default_names["message"])
    end
  end

  private

  def fetch_names
    begin
      JSON.parse(RestClient.get("https://dog.ceo/api/breeds/list/all").body)
    rescue Object => e
      default_names
    end
  end

  def build_list_for(names)
    list = []
    names.each do |name|
      if name.last.any?
        list << build_names_for(name)
      else
        list << name.first
      end
    end

    list.flatten.map(&:capitalize).sort
  end

  # User friendly name for index page
  def build_names_for(name)
    names = []
    name.last.each do |subname|
      names << "#{subname} #{name.first}"
    end
    names
  end

  def success?(names)
    names["status"] == "success"
  end

  def default_names
    {
      "status"  => "success",
      "message" => { "african" => [] }
    }
  end
end
