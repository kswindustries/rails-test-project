module BreedsHelper
  def breed_options_for_select
    DogBreedNameList
      .fetch
      .map { |name| [name, name] }
  end
end
